# Docker_install

#### 介绍
docker安装教程,主要介绍docker的安装和使用

#### 安装教程
1.windows 下安装docker，【请确保windows已开启虚拟化（在任务管理器-性能 查看）
    ，win10为64位】

    1-1 windows 专业版，企业版或教育版（内部版本15063或更高版本）
        1> 在【启用或关闭windows功能】中启用Hyper-v；
        2> 在官网下载docker安装包，安装完成即可

    1-2 windows 家庭版
        1> 以管理员身份运行Hyper-v.bat，中途会提示是否重启，输入"Y"重启电脑，电脑
        重启之后在【启用或关闭windows功能】中查看是否开启Hyper-v;
        2> 重复1-1步骤完成安装

2.在Ubuntu下安装docker
    
    Docker 在 get.docker.com 和 test.docker.com 上提供了方便脚本，用于将快速安装 
    Docker Engine-Community 的边缘版本和测试版本。脚本的源代码在 docker-install 
    仓库中。 不建议在生产环境中使用这些脚本，在使用它们之前，您应该了解潜在的风险：

    脚本需要运行 root 或具有 sudo 特权。因此，在运行脚本之前，应仔细检查和审核脚本。

    这些脚本尝试检测 Linux 发行版和版本，并为您配置软件包管理系统。此外，脚本不允许
    您自定义任何安装参数。从 Docker 的角度或您自己组织的准则和标准的角度来看，这可能
    导致不支持的配置。

    这些脚本将安装软件包管理器的所有依赖项和建议，而无需进行确认。这可能会安装大量软
    件包，具体取决于主机的当前配置。

    该脚本未提供用于指定要安装哪个版本的 Docker 的选项，而是安装了在 edge
     通道中发布的最新版本。

    如果已使用其他机制将 Docker 安装在主机上，请不要使用便捷脚本。


    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sudo sh get-docker.sh
    
    安装完成之后执行：
    service docker start
    启动docker服务。
